import java.util.Random;
import java.util.Scanner;

public class Practica_2 {

	public static void main(String[] args) {
		
		//inicialització de variables
		int maxIntents = 10;
		int intents = 0;
		boolean paraulaEncertada = false;
		
		//agafem una paraula de la array
		String paraula = findWord();
		String msg = "";
		//preparem la paraula amagada
		for(int i = 0; i < paraula.length(); i++){
			msg = msg + "*";
		}
		
		StringBuilder msg2 = new StringBuilder(msg);
		
		System.out.println("Endevina la paraula");
		
		char lletra;
		//bucle del joc
		while(!paraulaEncertada && intents < maxIntents){
			//enunciat de cada torn + obtenim lletra per teclat
			lletra = showMessage(intents, msg2);
			//mirem si la lletra esta en la paraula
			comprovaLletra(paraula, msg2, lletra);
			//comprovem si hem endevinat tota la paraula
			paraulaEncertada = comprovaFinal(paraula, msg2);
			//sumem intents
			intents++;
			if(!paraulaEncertada){
				//dibuixem la figura si no hem encertat encara la paraula
				printForca(intents);
			}
		}
		//comprovació final
		if(paraulaEncertada){
			System.out.println("Victoria!");
		}else{
			System.out.println("Has perdut!");
		}

	}
	
	public static boolean comprovaFinal(String paraula, StringBuilder msg){
		if(paraula.equals(msg.toString())){
			return true;
		}
		return false;
	}
	
	public static void comprovaLletra(String paraula, StringBuilder msg, char lletra){
		boolean encert = false;
		
		for(int i = 0; i < paraula.length(); i++){
			if(lletra == paraula.charAt(i)){
				if(lletra != msg.charAt(i)){
					if(!encert){
						System.out.println("Has encertat la lletra");
						encert=true;
					}
					msg.setCharAt(i, lletra);
				}else if(!encert){
					System.out.println("Ja havies encertat aquesta lletra");
					encert=true;
				}
			}
		}
		if(!encert){
			System.out.println("No has encertat la lletra");
		}
	}
	
	public static String findWord(){
		Random rnd = new Random();
		
		String[] manyOptions = new String[10];
		manyOptions[0] = "class";
		manyOptions[1] = "glass";
		manyOptions[2] = "school";
		manyOptions[3] = "freeze";
		manyOptions[4] = "tiger";
		manyOptions[5] = "paper";
		manyOptions[6] = "pen";
		manyOptions[7] = "mobile";
		manyOptions[8] = "wall";
		manyOptions[9] = "fidget";
		
		int random = rnd.nextInt(10);
		
		return manyOptions[random];
	}
	
	public static char showMessage(int iI, StringBuilder msg){
		System.out.println("Progres: " + msg);
		System.out.println("Intent " + (iI+1) + ", entra una lletra >");
		Scanner inputDevice = new Scanner( System.in );
		char lletra = inputDevice.next().charAt(0);
		return lletra;
	}
	
	public static void printForca(int iI){
		System.out.println("\n");
		switch(iI){
			case 1: 
				System.out.println("\n" + "\n" + "\n" + "\n" + "_____");
				break;
			case 2:
				System.out.println("\n" + "|\n" + "|\n" + "|\n" + "|_____");
				break;
			case 3: 
				System.out.println("____\n" + "|\n" + "|\n" + "|\n" + "|_____");
				break;
			case 4:
				System.out.println("____\n" + "|  o\n" + "|\n" + "|\n" + "|_____");
				break;
			case 5: 
				System.out.println("____\n" + "|  o\n" + "| /\n" + "|\n" + "|_____");
				break;
			case 6:
				System.out.println("____\n" + "|  o\n" + "| /0\n" + "|\n" + "|_____");
				break;
			case 7:
				System.out.println("____\n" + "|  o\n" + "| /0/\n" + "|\n" + "|_____");
				break;
			case 8: 
				System.out.println("____\n" + "|  o\n" + "| /0/\n" + "| /\n" + "|_____");
				break;
			case 9: 
				System.out.println("____\n" + "|  o\n" + "| /0/\n" + "| / /\n" + "|_____");
				break;
			case 10: 
				System.out.println("____\n" + "|  o\n" + "| /0/\n" + "| /./\n" + "|_____");
				break;
			default: break;
		}
		System.out.println("\n");
	}
	
	
	
	
	
	
	
	
	
	
}